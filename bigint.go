package main

import (
	"fmt"
	"strconv"
)

type bigint struct {
	num string
}

func NewInt(num string) bigint {
	return bigint{num}
}

func main() {
	a := NewInt("988847123412385995937737458959")
	b := NewInt("“21231231231231231231231231233")
	// fmt.Println(b.Set("1"))
	fmt.Printf("Add: %v", Add(a, b))
}

func Add(str1, str2 bigint) bigint {
	if len(str1.num) > len(str2.num) {
		t := str1
		str1 = str2
		str2 = t
	}
	str := ""
	length1 := len(str1.num)
	length2 := len(str2.num)

	str1 = NewInt(reverse(str1))
	str2 = NewInt(reverse(str2))

	carried := 0
	sum := 0
	// sum of current digits and carried
	for i := 0; i < length1; i++ {
		k := int(str1.num[i] - 48)
		j := int(str2.num[i] - 48)
		sum = (k + j + carried)
		str += strconv.Itoa(sum % 10)
		carried = sum / 10
		sum = 0
	}
	// Add remaining digits of larger number
	for i := length1; i < length2; i++ {
		j := (str2.num[i] - 48)
		sum = int(j) + carried
		str += strconv.Itoa(sum % 10)
		carried = sum / 10
	}
	// Adding remaining carried
	if carried > 0 {
		str += strconv.Itoa(carried)
	}

	str = reverse(NewInt(str))
	return NewInt(str)

}

// func (z *bigint) Set(str string) error {
// 	_, err := strconv.Atoi(str)
// 	z.num = str
// 	return err
// }

func reverse(s bigint) (result string) {
	for _, v := range s.num {
		result = string(v) + result
	}
	return
}
